---
title: Company Details
date: 2012-06-07 21:47:33
---
Information in accordance with §5 TMG:

Stefan Hojer IT-Beratung  
Altusrieder Straße 33  
87439 Kempten  
Germany

Responsible for content (according §55 II RStV): Stefan Hojer (address see above)

## Contact Information

Telephone: +49 176 24068150  
E-Mail: mail@stefanhojer.de  
Website: https://www.stefanhojer.de

## VAT number

VAT indentification number ("USt-Id") in accordance with §27a of the German
VAT act

DE327317045

## Disclaimer

### Accountability for links

Responsibility for the content of external links (to web pages of third
parties) lies solely with the operators of the linked pages. We checked the
links carefully and no violations were evident to us at the time of linking.
Should any legal infringement become known to us, we will remove the
respective link immediately.

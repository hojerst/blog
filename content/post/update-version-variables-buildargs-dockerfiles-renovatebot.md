---
title: "Update Version in variables or buildargs in Dockerfiles with Renovatebot"
date: 2021-06-17T01:29:40+02:00
categories:
- Programming 
tags:
- dependency management
---

My team is using [Renovatebot](https://www.whitesourcesoftware.com/free-developer-tools/renovate/) for automatic
dependency management and is quite happy with it.

Renovate can manage a bunch of dependencies out of the box:
 * Tags and Digests of Docker images
 * Refs of GitLab Include directives
 * Ansible Collections / Roles
 * Helm Charts
 * Python packages
 * [and more...](https://docs.renovatebot.com/modules/manager/)

However, we have some versions in a `Dockerfile` which we use for downloading packages during the build process:

```Dockerfile
FROM alpine:3.14.0@sha256:234cb88d3020898631af0ccbbcca9a66ae7306ecd30c9720690858c1b007d2a0

ARG KUBECTL_VERSION=1.21.1

RUN wget -q -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod 0755 /usr/local/bin/kubectl
```

The `KUBECTL_VERSION` variable isn't picked up by Renovatebot automatically, as it has no way of knowing which package
this variable references. However you can use the `regex` manager in the `.renovate.json` file to provide some context.

<!--more-->

The Renovatebot documentation actually provides an [example how to achieve this](https://docs.renovatebot.com/modules/manager/regex/#advanced-capture).
However, we needed some further customization:

 * ability to remove the `v` from tags on github (e.g. `v1.2.3 -> 1.2.3`).
 * support for `ARG` and `ENV` keyword

We came up with the following solution which can be added to the `.renovate.json`: 

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "regexManagers": [
    {
      "fileMatch": [
        "(^|/)Dockerfile$",
        "(^|/)Dockerfile\\.[^/]*$"
      ],
      "matchStrings": [
        "#\\srenovate:\\sdatasource=(?<datasource>.*?) depName=(?<depName>.*?)( versioning=(?<versioning>.*?))?( extractVersion=(?<extractVersion>.*?))?\\s(ENV|ARG) .*?_VERSION=(?<currentValue>.*)\\s"
      ]
    }
  ]
}
```

Using the snippet above, renovate will be able to manage the version of the variable(s) in the `Dockerfile`, just
by adding a special comment in front of the `ARG`/`ENV` statement:

```Dockerfile
FROM alpine:3.14.0@sha256:234cb88d3020898631af0ccbbcca9a66ae7306ecd30c9720690858c1b007d2a0

# renovate: datasource=github-releases depName=kubernetes/kubernetes extractVersion=^v(?<version>.*)$
ARG KUBECTL_VERSION=1.21.1

RUN wget -q -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod 0755 /usr/local/bin/kubectl
```
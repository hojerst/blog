FROM klakegg/hugo:0.111.3-ext-ci@sha256:594a5a74d6b32423292522060a8a1fc11fc1303e0a62bcc9e24edeaad12475d1 AS build

COPY . /app
WORKDIR /app

RUN hugo

FROM nginx:1.27-alpine@sha256:4ff102c5d78d254a6f0da062b3cf39eaf07f01eec0927fd21e219d0af8bc0591

COPY --from=build /app/public /usr/share/nginx/html

# see: https://github.com/opencontainers/image-spec/blob/v1.0.1/annotations.md
LABEL org.opencontainers.image.title="Blog"
LABEL org.opencontainers.image.description="redcube.de Blog website"
LABEL org.opencontainers.image.licenses="MIT"
